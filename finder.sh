#!/bin/bash

NEWLOC=`curl -q -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' \
     -I 'http://go.microsoft.com/fwlink/?LinkID=229322' -L 2>/dev/null \
          | grep Location | grep -v go.microsoft.com | sed 's/Location: //' | tr -d '\r'` 

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
